/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 21:54:21 by osapon            #+#    #+#             */
/*   Updated: 2018/11/02 21:54:24 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_fdf		map_width(t_fdf fdf)
{
	double	i;
	t_fdf	tmp;

	i = 0;
	tmp = fdf;
	while (fdf.map)
	{
		i++;
		fdf.map = fdf.map->next;
	}
	tmp.w = i;
	return (tmp);
}

t_fdf		*map_height(t_fdf *fdf)
{
	double	i;
	t_fdf	*tmp;

	i = 0;
	tmp = fdf;
	while (fdf)
	{
		i++;
		fdf = fdf->next;
	}
	tmp->h = i;
	return (tmp);
}

t_map		*connect_eyses(t_map *fdf, t_map *next)
{
	t_map	*tmp;

	tmp = fdf;
	while (tmp)
	{
		tmp->print_color = tmp->color;
		tmp->yese = next;
		tmp = tmp->next;
		next = next->next;
	}
	return (fdf);
}

t_fdf		*ft_fdf(t_fdf *fdf)
{
	t_fdf	*tmp;

	*fdf = map_width(*fdf);
	fdf = map_height(fdf);
	tmp = fdf;
	tmp->rot_x = -1;
	tmp->rot_z = 0.1;
	tmp->rot_y = -0.5;
	fdf->zoom = ((WINW + WINH) / (fdf->w + fdf->h * 2));
	tmp->move_top = WINH;
	tmp->move_bottom = WINW;
	while (fdf->next)
	{
		fdf->map = connect_eyses(fdf->map, fdf->next->map);
		fdf = fdf->next;
	}
	return (tmp);
}

t_fdf		*to_zero(t_fdf *fdf)
{
	t_fdf	*tmp;
	t_map	*map;

	tmp = fdf;
	while (fdf)
	{
		map = fdf->map;
		while (map)
		{
			map->dx = map->x - (tmp->w - 1) / 2;
			map->dy = map->y - (tmp->h - 1) / 2;
			map->dz = map->z;
			map = map->next;
		}
		fdf = fdf->next;
	}
	return (tmp);
}
