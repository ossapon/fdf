/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 21:35:23 by osapon            #+#    #+#             */
/*   Updated: 2018/07/29 18:17:03 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "fdf.h"

int			red_cross(void)
{
	exit(0);
}

t_fdf		*init_img(t_fdf *fdf)
{
	fdf = to_zero(fdf);
	fdf = matrix(fdf);
	fdf = ft_save_pixels(fdf);
	mlx_put_image_to_window(fdf->img->ptr, fdf->img->window,
						fdf->img->img, 0, 0);
	ft_bzero(fdf->prev->data, (WINW * WINH) * 4);
	return (fdf);
}

int			main(int ac, char **av)
{
	t_fdf	*fdf;
	char	*line;
	int		fd;

	fd = open(av[1], O_RDONLY);
	if (ac == 2 && read(fd, &line, 1) > 0)
	{
		fdf = get_data(av[1], NULL);
		fdf->img = ft_memalloc(sizeof(t_img));
		fdf->prev = ft_memalloc(sizeof(t_img));
		ft_init(fdf);
		fdf = ft_fdf(fdf);
		fdf = init_img(fdf);
		mlx_hook(fdf->img->window, 2, 5, hook, fdf);
		mlx_hook(fdf->img->window, 17, 0, red_cross, 0);
		mlx_loop(fdf->img->ptr);
	}
	else
		ft_printf("usage: ./fdf [file]\n");
	return (0);
}
