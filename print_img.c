/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_img.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 21:57:21 by osapon            #+#    #+#             */
/*   Updated: 2018/11/02 21:57:23 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		draw_x(t_map *map, t_map *next, t_img *img, int color)
{
	int		x;
	int		y;
	int		i;
	int		d;

	d = (int)(((int)fabs(next->dy - map->dy) << 1) - fabs(next->dx - map->dx));
	x = (int)map->dx + (next->dx >= map->dx ? 1 : -1);
	y = (int)map->dy;
	i = 0;
	while (++i <= (int)fabs(next->dx - map->dx))
	{
		if (d > 0)
		{
			d += ((int)fabs(next->dy - map->dy) -
				(int)fabs(next->dx - map->dx)) << 1;
			y += (next->dy >= map->dy) ? 1 : -1;
		}
		else
			d += (int)fabs(next->dy - map->dy) << 1;
		img_put_pixel(img, x, y, color);
		x += (next->dx >= map->dx ? 1 : -1);
	}
}

void		draw_y(t_map *map, t_map *next, t_img *img, int color)
{
	int		x;
	int		y;
	int		i;
	int		d;

	d = (int)(((int)fabs(next->dx - map->dx) << 1) - fabs(next->dy - map->dy));
	y = (int)map->dy + ((int)next->dy >= (int)map->dy ? 1 : -1);
	x = (int)map->dx;
	i = 0;
	while (++i <= (int)fabs(next->dy - map->dy))
	{
		if (d > 0)
		{
			d += ((int)(fabs(next->dx - map->dx) -
						(int)fabs(next->dy - map->dy))) << 1;
			x += ((int)next->dx >= (int)map->dx) ? 1 : -1;
		}
		else
			d += (int)(fabs(next->dx - map->dx)) << 1;
		img_put_pixel(img, x, y, color);
		y += ((int)next->dy >= (int)map->dy ? 1 : -1);
	}
}

void		draw_line(t_map *map, t_map *next, t_img *img, int color)
{
	if ((int)fabs(next->dy - map->dy) <= (int)fabs(next->dx - map->dx))
		draw_x(map, next, img, color);
	else
		draw_y(map, next, img, color);
}

t_fdf		*ft_save_pixels(t_fdf *fdf)
{
	t_fdf	*tmp;
	t_map	*map;

	tmp = fdf;
	while (fdf)
	{
		map = fdf->map;
		while (map)
		{
			img_put_pixel(tmp->img, (int)(map->dx), (int)map->dy, map->color);
			if (map->next && (map->print_color = tmp->map->print_color))
				draw_line(map, map->next, tmp->img, map->print_color);
			if (map->yese && (map->print_color = tmp->map->print_color))
				draw_line(map, map->yese, tmp->img, map->print_color);
			map = map->next;
		}
		fdf = fdf->next;
	}
	return (tmp);
}
