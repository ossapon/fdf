/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/02 21:53:22 by osapon            #+#    #+#             */
/*   Updated: 2018/11/02 21:53:27 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		center(t_fdf *fdf)
{
	fdf->zoom = (unsigned int)((WINW + WINH) / (fdf->w + fdf->h * 2));
	fdf->move_top = WINH;
	fdf->move_bottom = WINW;
	fdf->rot_x = -1;
	fdf->rot_z = 0.1;
	fdf->rot_y = -0.5;
	fdf->map->print_color = fdf->map->color;
	ft_bzero(fdf->img->data, (WINW * WINH) * 4);
}

int			hook(int key, t_fdf *fdf)
{
	if (key == 2 || key == 0)
		fdf->rot_x += (key == 2) ? 0.01 : -0.01;
	if (key == 1 || key == 13)
		fdf->rot_y -= (key == 1) ? 0.01 : -0.01;
	if (key == 12 || key == 14)
		fdf->rot_z += (key == 12) ? 0.01 : -0.01;
	if (key == 18)
		fdf->map->print_color += 1000;
	if (key == 44 || key == 47)
		fdf->zoom -= (key == 44) ? 1 : -1;
	if (key == 49)
		center(fdf);
	if (key == 53)
		exit(0);
	if (key == 123 || key == 124)
		fdf->move_bottom -= (key == 123) ? 15 : -15;
	if (key == 125 || key == 126)
		fdf->move_top += (key == 125) ? 10 : -10;
	fdf->zoom = (fdf->zoom == 0) ? 1 : fdf->zoom;
	fdf = init_img(fdf);
	return (0);
}

void		get_exes(t_map *now, t_fdf *rot)
{
	double	x;
	double	y;
	double	x1;
	double	y1;

	x = now->dx;
	y = now->dy;
	now->dx = x;
	now->dy = y * cos(rot->rot_x) + now->dz * sin(rot->rot_x);
	now->dz = (-y) * sin(rot->rot_x) + now->dz * cos(rot->rot_x);
	x1 = now->dx * cos(rot->rot_y) - now->dz * sin(rot->rot_y);
	y1 = now->dy;
	now->dx = x1 * cos(rot->rot_z) + y1 * sin(rot->rot_z);
	now->dy = y1 * cos(rot->rot_z) - x1 * sin(rot->rot_z);
}

t_fdf		*matrix(t_fdf *fdf)
{
	t_fdf	*tmp;
	t_map	*map;

	tmp = fdf;
	while (fdf)
	{
		map = fdf->map;
		while (map)
		{
			get_exes(map, tmp);
			map->dx = (map->dx * tmp->zoom) + (tmp->move_bottom / 2);
			map->dy = (map->dy * tmp->zoom) + (tmp->move_top / 2);
			map = map->next;
		}
		fdf = fdf->next;
	}
	return (tmp);
}
