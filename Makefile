NAME = fdf

FLAGS = -lmlx -framework OpenGL -framework AppKit -Wall -Wextra -Werror -o

SRCS = main.c print_img.c create_map.c matrix.c get_data_functions.c

OBJ=$(SRCS:.c=.o)

all: $(NAME)

$(NAME) : $(OBJ)
		make -C libft/
		gcc $(FLAGS) $(NAME) $(SRCS)  -I /bin/ -L./libft -lft
clean:
		make -C libft/ clean
		/bin/rm -f $(OBJ)
fclean: clean
		rm -f libft/libft.a
		/bin/rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re norm
		.NOTPARALLEL: all clean fclean re norm

norm:
		norminette *.c* .h