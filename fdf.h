/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 21:36:15 by osapon            #+#    #+#             */
/*   Updated: 2018/07/23 19:37:05 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define WINW	1980
# define WINH	1280

# include "minilibx_macos/mlx.h"
# include "libft/ft_printf.h"
# include "libft/get_next_line.h"
# include <math.h>

typedef struct		s_map
{
	int				print_color;
	int				color;
	double			y;
	double			x;
	double			z;
	double			dy;
	double			dx;
	double			dz;
	struct s_map	*next;
	struct s_map	*yese;
}					t_map;

typedef struct		s_img
{
	void			*img;
	int				width;
	int				height;
	int				bpp;
	int				endian;
	int				size_line;
	char			*data;
	void			*ptr;
	void			*window;
}					t_img;

typedef struct		s_fdf
{
	double			w;
	double			h;
	double			rot_x;
	double			rot_z;
	double			rot_y;
	double			move_top;
	double			move_bottom;
	unsigned int	zoom;
	t_map			*map;
	t_img			*prev;
	struct s_fdf	*next;
	t_img			*img;
}					t_fdf;

t_fdf				*get_data(char *av, t_fdf *fdf);
t_fdf				*ft_save_pixels(t_fdf *fdf);
void				ft_init(t_fdf *fdf);
t_fdf				*init_img(t_fdf *fdf);
t_fdf				*matrix(t_fdf *fdf);
t_fdf				*ft_fdf(t_fdf *fdf);
t_fdf				*to_zero(t_fdf *fdf);
int					hook(int key, t_fdf *fdf);
void				img_put_pixel(t_img *image, int x, int y, int color);

#endif
