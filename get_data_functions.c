/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_data_functions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 22:59:23 by osapon            #+#    #+#             */
/*   Updated: 2018/09/17 22:59:03 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "fdf.h"

void			ft_init(t_fdf *fdf)
{
	fdf->img->ptr = mlx_init();
	fdf->img->window = mlx_new_window(fdf->img->ptr, WINW, WINH, "lol kek fdf");
	fdf->img->img = mlx_new_image(fdf->img->ptr, WINW, WINH);
	fdf->img->width = WINW;
	fdf->img->height = WINH;
	fdf->img->data = mlx_get_data_addr(fdf->img->img,
			&fdf->img->bpp, &fdf->img->size_line, &fdf->img->endian);
	fdf->prev->ptr = mlx_init();
	fdf->prev->img = mlx_new_image(fdf->img->ptr, WINW, WINH);
	fdf->prev->width = WINW;
	fdf->prev->height = WINH;
	fdf->prev->data = mlx_get_data_addr(fdf->img->img,
				&fdf->img->bpp, &fdf->img->size_line, &fdf->img->endian);
}

void			img_put_pixel(t_img *image, int x, int y, int color)
{
	if (x < 0 || x >= image->width || y < 0 || y >= image->height)
		return ;
	*(int *)(image->data + (x + y * image->width)
		* image->bpp / 8) = color - 100;
}

int				text_val(char *line)
{
	static int	op;
	int			i;

	i = op;
	op = 0;
	while (*line)
	{
		if (*line == ',')
			while (*(++line) && *line != ' ')
				if (!ft_isalnum(*line))
					return (0);
		if (!ft_isdigit(*line) && *line != ' ' && *line == ' '
			&& *(line + 1) == ' ' && *(line + 1) == '\0'
			&& !ft_isdigit(*(line + 1)))
			return (0);
		if (ft_isdigit(*line) && (*(line + 1) == '\0' ||
								*(line + 1) == ' ' || *(line + 1) == ','))
			op++;
		line++;
	}
	if (i != 0 && i != op)
		return (0);
	return (1);
}

t_map			*get_coords(char *line, t_map *es, int *y)
{
	int			i;
	int			x;
	char		**coords;
	t_map		*tmp;

	coords = ft_strsplit(line, ' ');
	x = 0;
	i = -1;
	while (coords[++i])
	{
		if (!es && (tmp = (t_map*)ft_memalloc(sizeof(t_map))))
			es = tmp;
		else if (es && (tmp->next = (t_map*)ft_memalloc(sizeof(t_map))))
			tmp = tmp->next;
		tmp->y = *y;
		tmp->x = x;
		tmp->z = ft_atoi(coords[i]);
		tmp->color = (ft_strchr(coords[i], ',')) ?
				(int)ft_atoi_base(ft_strchr(coords[i], ',') + 1, 16)
					: (int)ft_atoi_base("0xab274f", 16);
		x++;
	}
	ft_faop((void**)coords);
	(*y)++;
	return (es);
}

t_fdf			*get_data(char *av, t_fdf *fdf)
{
	int			fd;
	char		*line;
	t_fdf		*tmp;
	static int	y;

	fd = open(av, O_RDONLY);
	tmp = NULL;
	y = 0;
	while (get_next_line(fd, &line) > 0)
	{
		if (!text_val(line))
			exit(ft_printf("wrong data input at line: %d\n", y));
		else
		{
			if (!fdf && (tmp = (t_fdf*)ft_memalloc(sizeof(t_fdf)))
				&& (tmp->map = get_coords(line, tmp->map, &y)))
				fdf = tmp;
			else if (fdf && (tmp->next = (t_fdf*)ft_memalloc(sizeof(t_fdf)))
				&& (tmp->next->map = get_coords(line, tmp->next->map, &y)))
				tmp = tmp->next;
			ft_strdel(&line);
		}
	}
	close(fd);
	return (fdf);
}
